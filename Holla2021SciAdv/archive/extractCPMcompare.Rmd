---
title: "Poe et al and Holla et al: Extract CPM and compare"
author: "DCI Bioinformatics"
date: '`r format(Sys.Date(), "%B %d, %Y")`'
output: 
  html_document:
    toc: true
    toc_depth: 2
    toc_float: true
    code_folding: hide
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE, warning = FALSE, cache = T, cache.lazy = FALSE)
```

# Prerequisites

Load packages
```{r pkg}
library(tidyverse)
library(readxl)
library(kableExtra)
library(Seurat)
library(yaml)
library(viridis)
library(circlize)
library(ComplexHeatmap)
library(RColorBrewer)
library(ggrepel)
```

Load paths
```{r filedirs}
#### on plp
#configfile <- 'config.yml'
#myconf <- yaml::read_yaml(configfile)
#procdir <- myconf$procdir
#codedir <- myconf$codedir
#gtfdir <- myconf$gtfdir

#### for plotting locally
#getwd()
codedir <- file.path("Holla2021SciAdv")
gtfdir <- procdir <- file.path(codedir,"data")
outdir <- file.path(codedir, "proc")

gfile <- file.path(
  gtfdir,'gencode.v24.chr_patch_hapl_scaff.basic.annotation.gtf'
)
tools::md5sum(gfile)
xlfile <- file.path(codedir,"data","Signature gene list for new figure_v3-18-2022_revised.xlsx")

ncellsf <- file.path(codedir,"data","ncells.csv")
```

Import Utility functions
```{r utilfun}
utilfname <- file.path(codedir, "util.R") 
utilfname
tools::md5sum(utilfname)
source(utilfname)

# function to examine data slots
dimSlots <- function(obj){
  
  dim.list <- list()
  
  # RNA
  dim.list[['RNA.counts']] <- dim(obj$RNA@counts)
  dim.list[['RNA.data']] <- dim(obj$RNA@data)
  dim.list[['RNA.scale.data']] <- dim(obj$RNA@scale.data)
  
  if("integrated" %in% Assays(obj)){
  # integrated
  dim.list[['integrated.counts']] <- dim(obj$integrated@counts)
  dim.list[['integrated.data']] <- dim(obj$integrated@data)
  dim.list[['integrated.scale.data']] <- dim(obj$integrated@scale.data)  
  }
  
  return(dim.list)
}

# function to recode seurat object metadata related to data cohorts
recode_cohortMeta <- function(seu.obj){
  # identify datasets that were integrated, "cohort"
  # identify datasets by condition, "exp1"
  # combine into cohort.exp1
  seu.obj@meta.data %>%
    mutate(cohort = ifelse(study == "Holla", paste0(study,".", exp1),
                           ifelse(study == "Poe", paste0(study, ".", runid),
                                  NA))) %>%
    mutate(exp1 = factor(exp1, levels = c("Healthy", "HIV","Malaria",
                                          "no_cGVHD","act_cGVHD"))) %>%
    mutate(cohort.exp1 = ifelse(study == "Holla", cohort,
                                ifelse(study == "Poe", paste0(cohort, ".", exp1),
                                       NA)),
           cohort.exp1 = factor(cohort.exp1, levels = c("Poe.1.no_cGVHD","Poe.1.act_cGVHD", 
                                         "Poe.2.no_cGVHD","Poe.2.act_cGVHD",
                                         "Holla.Healthy", "Holla.Malaria","Holla.HIV"))) -> md
  # md %>%  
  #   group_by(study, runid, cohort, exp1, cohort.exp1) %>%
  #   summarize(n = length(sampid))
  
  # add updated metadata to the seurat object
  md %>%
    dplyr::select(cohort, exp1, cohort.exp1) -> newcols
  seu.obj %>%
    Seurat::AddMetaData(metadata = newcols) -> seu.obj.updated
  
  return(seu.obj.updated)
}

# function to recode seurat object metadata based on itgax/cd27 expression
recode_itgax.cd27 <- function(seu.obj, keygenes.indx, data.slot = "integrated"){
  
  if(data.slot == "integrated"){
    # get integrated data
    DefaultAssay(object = seu.obj) <- "integrated"
    thres <- 0.5
  }
  thres <- 50
  FetchData(object = seu.obj, slot = "data", keygenes.indx$geneid) %>%
    rownames_to_column(var = "cell.name") %>%
    pivot_longer(cols = -c(cell.name),
                 names_to = "geneid",
                 values_to = "int.data") -> int.data.l
  # get metadata
  seu.obj@meta.data %>%
    rownames_to_column(var = "cell.name") -> md
  
  # combine and reshape, refine metadata
  # Identify cells as (i) +ITGAX -CD27, (ii) -ITGAX +CD27, (iii) +ITGAX +CD27
  int.data.l %>%
    left_join(md) %>%
    left_join(keygenes.indx) %>%
    pivot_wider(id_cols = c(cell.name),
                names_from = gene_name,
                values_from = int.data) %>%
    mutate(itgax.plus = ITGAX > thres,
           cd27.plus = CD27 > thres,
           itgax.cd27 = ifelse(itgax.plus == TRUE & cd27.plus == TRUE, "+ITGAX.+CD27",
                               ifelse(itgax.plus == TRUE & cd27.plus == FALSE, "+ITGAX.-CD27",
                                      ifelse(itgax.plus == FALSE & cd27.plus == TRUE, "-ITGAX.+CD27",
                                             "other")))) -> md.out
  
  # add updated metadata to the seurat object
  md %>%
    left_join(md.out) %>%
    dplyr::select(cell.name, CD27, ITGAX, PAX5, itgax.plus, cd27.plus, itgax.cd27) %>%
    column_to_rownames(var = "cell.name") -> newcols
  seu.obj %>%
    Seurat::AddMetaData(metadata = newcols) -> seu.obj.updated
  
  return(seu.obj.updated)
  
}

# for heatmaps
scale_rows = function(x){
    m = apply(x, 1, mean, na.rm = T)
    s = apply(x, 1, sd, na.rm = T)
    return((x - m) / s)
}

# downsample the number of cells to display in heatmap
make_downsamp_heatmap <- function(seed, n.cells = NULL, plotdf1.filt, title){
  
  #seed = 1
  #n.cells = NULL
  #plotdf1.filt
  #title = "-ITGAX.+CD27"
  
  if(!is.null(n.cells)){
    # downsample cells by exp1.pretty group
  set.seed(seed)
  plotdf1.filt %>%
    dplyr::select(cell.name, exp1) %>%
    distinct() %>%
    group_by(exp1) %>%
    slice_sample(n = n.cells, replace = F) -> selcells
  
  # extract data matrix to plot
  plotdf1.filt %>%
    filter(cell.name %in% selcells$cell.name) -> mat.df
  }else{
    plotdf1.filt %>%
      dplyr::select(cell.name, exp1) %>%
      distinct() -> selcells
    mat.df <- plotdf1.filt
  }
  
  mat.df %>%
    pivot_wider(id_cols = gene_name,
                names_from = cell.name,
                values_from = int.data) -> mat
  x.new <- as.matrix(mat[,-c(1)])
  rownames(x.new) <- mat$gene_name

  # create column annotation df
  data.frame(cell.name = colnames(x.new)) %>%
    left_join(selcells) %>%
    mutate(grp = factor(exp1, levels =  c("act_cGVHD","no_cGVHD","Healthy","HIV","Malaria"))) %>%
    remove_rownames() %>%
    column_to_rownames(var = "cell.name") %>%
    arrange(exp1) -> mat.ann

  # re-order columns based on trt
  x.new %>%
    as_tibble() %>%
    relocate(row.names(mat.ann)) %>%
    as.matrix -> out
  row.names(out) <- rownames(x.new)
  
  # check for rows(genes) with 0 variation between samples
  out %>%
    data.frame(.) %>%
    rowwise() %>%
    mutate(var = var(c_across(everything()))) -> out.ov
  zeroVar.genes <- row.names(out[out.ov$var == 0,]) # genes with 0 variation across cells
  print(paste0("Genes removed because 0 variation across cells: ", toString(zeroVar.genes)))
  out.ovo <- out[out.ov$var != 0,]
  
  # scale by row
  out.ovo.s <- scale_rows(out.ovo)
  
  # save everything needed for plotting
  res <- list(mat = out.ovo.s, mat.ann = mat.ann, zeroVar.genes = zeroVar.genes)
  
  # set up color scale
  min.val <- floor(range(res$mat)[1])
  max.val <- ceiling(range(res$mat)[2])
  col_fun = colorRamp2(c(min.val, 0, max.val), c("blue", "white", "red"))
  
  # plot
  ha = HeatmapAnnotation(foo = anno_block(gp = gpar(fill = "gray"), 
                                          labels = levels(res$mat.ann$grp),
                                          labels_gp = gpar(fontsize = 8),
                                          labels_rot = 45),
                         annotation_height = unit(1,"lines"))
  p <- Heatmap(res$mat, col = col_fun, name = "rel. expr.",
               column_title = title,
               top_annotation = ha,
               cluster_rows = TRUE,
               cluster_columns = TRUE,
               cluster_column_slices = FALSE,
               column_dend_side = "bottom",
               column_split = res$mat.ann$grp,
               show_column_dend = FALSE,
               show_column_names = FALSE,
               row_dend_side = "right",
               row_names_side = "left",
               row_names_gp = gpar(fontsize = 6),
               column_title_gp = gpar(fontsize = 10),
               show_row_dend = TRUE)
p
  
res.out <- c(res, p=p)
  return(res.out)
}

```

Import gtf file and heatmap genes
```{r}
# # Import gtf file
# rtracklayer::import(gfile) %>%
#   plyranges::filter(type == "gene") %>%
#   tibble::as_tibble() %>%
#   plyranges::select(gene_id, gene_name) %>%
#   dplyr::mutate(geneid = gsub(pattern = "\\.\\d+$", "", gene_id)) -> gtf
# 
# # Import heatmap gene list
# readxl::read_xlsx(
#   xlfile,
#   skip = 1,
#   col_names = "gene_name"
# ) %>%
#   pull(gene_name) %>% unique() -> glist
# #length(glist) # 126 unique symbols
# # Make sure that PAX5, ITGAX, and CD27 are present in the heatmap gene list. Need to add PAX5
# #c("PAX5","ITGAX","CD27") %in% glist
# glist.ann <- union(glist, 'PAX5') #' Add PAX5
# 
# # Use the GTF to annotate the gene symbols with Ensembl IDs
# 
# # Check that all glist symbols are in the gtf
# glist.ann[!glist.ann %in% gtf$gene_name]
# # Add ensembl ids to glist
# data.frame(gene_name = glist.ann) %>%
#   dplyr::inner_join(gtf, by = "gene_name") -> genepanel
# #dim(genepanel) #148

# # Identify cases where there are multiple ensembl ids for 1 gene symbol. 12 gene symbols.
# genepanel %>%
#   group_by(gene_name) %>%
#   summarize(n = length(unique(gene_id)),
#             cat.geneid = paste0(gene_id, collapse = "__")) %>%
#   dplyr::filter(n != 1) %>%
#   dplyr::arrange(n) -> redund.symb # 12 gene_names
# redund.symb
# 
# #Identify cases where there are multiple gene symbols for 1 ensembl id. None.
# genepanel %>%
#   group_by(gene_id) %>%
#   summarize(n = length(unique(gene_name)),
#             cat.gene_name = paste0(gene_name, collapse = "__")) %>%
#   dplyr::filter(n != 1) %>%
#   dplyr::arrange(n) # 0 gene_ids

#saveRDSobj(genepanel, outdir, "poe-holla")
genepanel <- readRDSobj(outdir,"genepanel-poe-holla.RDS")

keygenes <- c("PAX5","ITGAX","CD27")
genepanel %>%
  filter(gene_name %in% keygenes) -> keygenes.indx
```

Seurat objects that were created by subsetting counts to B-cells, **prior to integration**, see line 106 from `4_integrate-cohorts.Rmd`
```{r}
bcellobj <- readRDSobj(procdir, "bcellobj-poe-holla.RDS")
bcellobj %>%
  map(~recode_cohortMeta(.x)) -> seulist

#names(seulist)
#seulist %>%
  #map(~dimSlots(obj = .x))
```

# Normalize using CPM

RC: Relative counts. Feature counts for each cell are divided by the total counts for that cell and multiplied by the scale.factor. No log-transformation is applied. For counts per million (CPM) set scale.factor = 1e6

```{r}
# seuobj <- seulist[[1]]
# seuobj
# seuobj.n <- NormalizeData(seuobj, normalization.method = "RC", scale.factor = 1e6)
# 
# i<-2
# df <- data.frame(counts = seuobj.n@assays$RNA@counts[,i],
#                  cpm = seuobj.n@assays$RNA@data[,i]) 
# ggplot(df, aes(x = counts, y = cpm)) +
#   geom_point() +
#   geom_abline(slope = 1, intercept = 0, linetype = 2)
# 

cpm.seuobj <- function(seuobj){
  seuobj.out <- NormalizeData(seuobj, normalization.method = "RC", scale.factor = 1e6)
  return(seuobj.out)
}

seulist %>%
  map(~cpm.seuobj(.x)) -> seulist.cpm
```

# Classify cells by itgax and cd27 expression

Based on the CPM distribution, decided to use a threshold for ITGAX and CD27 "on"/"off" of 50 CPM (see blue dashed line).
```{r}
get_ic.df <- function(seuobj){
  
  FetchData(object = seuobj, slot = "data", keygenes.indx$geneid) %>%
    rownames_to_column(var = "cell.name") %>%
    pivot_longer(cols = -c(cell.name),
                 names_to = "geneid",
                 values_to = "cpm") -> df
  
  return(df)
  
}

# extract cpm for 3 key genes
seulist.cpm %>%
  map(~get_ic.df(.x)) -> df.list
names(df.list) <- names(seulist.cpm)

# make plotting df
df.list %>%
  map_dfr(~data.frame(.x), .id = "condition") %>%
  left_join(keygenes.indx) %>%
  filter(gene_name %in% c("ITGAX","CD27")) -> df.cpm

# find min non-zero value for on/off threshold
df.cpm %>%
  filter(cpm != 0) %>%
  group_by(condition) %>%
  summarize(min = min(cpm),
            med = median(cpm),
            max = max(cpm))
# There are no values lower than 83 that are non-zero, so let's use 50

# plot
df.cpm %>%
  ggplot(aes(x = condition, y = cpm)) +
  geom_point(position = position_jitter(), alpha = .5, pch = 16) +
  facet_grid(~gene_name) +
  geom_hline(yintercept = 50, linetype = 2, color = "blue")

```

Classify cells and update metadata
```{r}
# classify into ITGAX/CD27 groups
seulist.cpm %>%
  map(~recode_itgax.cd27(.x, keygenes.indx, 
                         data.slot = "data")) -> seulist.cpm1
```

# Set up 

Set up levels and colors
```{r}
seulist.cpm1 %>%
  map_dfr(~.x@meta.data) %>%
  dplyr::select(exp1, itgax.cd27) %>%
  distinct() %>%
  filter(itgax.cd27 != "other") %>%
  remove_rownames() %>%
  mutate(condition = factor(exp1, levels =  c("act_cGVHD","no_cGVHD","Healthy","HIV","Malaria"))) %>%
  mutate(genegrp = factor(itgax.cd27, levels =  c("-ITGAX.+CD27","+ITGAX.-CD27","+ITGAX.+CD27"))) %>%
  mutate(genegrp.condition = paste0(genegrp, condition)) %>%
  arrange(genegrp, condition) -> level.indx

level.indx %>%  
  pull(genegrp.condition) -> genegrp.condition.levels
level.indx %>%
  mutate(genegrp.condition = factor(genegrp.condition, levels = genegrp.condition.levels)) -> level.indx
level.indx

# add colors
df1 <- data.frame(condition.col =  c(brewer.pal(4, "Paired"), "#E5F5E0"), condition = levels(level.indx$condition))
df2 <- data.frame(genegrp.col =  brewer.pal(3, "Greys"), genegrp = levels(level.indx$genegrp))
level.indx %>%
  left_join(df1) %>%
  left_join(df2) -> level.indx

# pull out custom indices for heatmap plotting functions
condition.col <- level.indx$condition.col; names(condition.col) <- level.indx$genegrp.condition
genegrp.col <- level.indx$genegrp.col; names(genegrp.col) <- level.indx$genegrp.condition


condition.txt = c("act_cGVHD", "no_cGVHD","Healthy","HIV","Malaria",rep("", 10))
genegrp.txt = c("C", rep("",4),
                "I",rep("",4),
                "IC",rep("",4))
```

Extract CPM dataframe
```{r}
get_genepanel.df <- function(seuobj, genepanel){
  
  # get cpm
  FetchData(object = seuobj, slot = "data", genepanel$geneid) %>%
    rownames_to_column(var = "cell.name") %>%
    pivot_longer(cols = -c(cell.name),
                 names_to = "geneid",
                 values_to = "cpm") -> cpm.data.l
  
  # get metadata
  seuobj@meta.data %>%
    rownames_to_column(var = "cell.name") %>%
    dplyr::select(cell.name, exp1, itgax.cd27) -> md
  
  # combine and reshape, refine metadata
  cpm.data.l %>%
    left_join(md) %>%
    left_join(genepanel) %>%
  filter(itgax.cd27 != "other") %>%
  dplyr::select(cell.name, exp1, itgax.cd27,
                gene_name, cpm) -> plotdf

  return(plotdf)
  
}

# extract cpm for key genes
seulist.cpm1 %>%
  map(~get_genepanel.df(seuobj = .x, genepanel)) -> df.list
names(df.list) <- names(seulist.cpm1)
df.list %>%
  map_dfr(~data.frame(.x), .id = "cohort") -> df.list

# check the cell numbers
df.list %>%
  filter(gene_name == "ADA") %>%
  group_by(exp1, itgax.cd27) %>%
  summarize(n = length(cpm)) %>%
  arrange(itgax.cd27, exp1)
```


# Make gene boxplots

Plotting function
```{r}
# set up levels
df.list %>%
  left_join(level.indx) %>%
  mutate(condition = factor(condition, levels =  c("act_cGVHD","no_cGVHD","Healthy","HIV","Malaria"))) %>%
  mutate(genegrp = factor(genegrp, levels =  c("-ITGAX.+CD27","+ITGAX.-CD27","+ITGAX.+CD27"))) %>%
  mutate(genegrp.condition = factor(genegrp.condition, levels = level.indx$genegrp.condition)) -> plotdf.p

# set up colors
level.indx %>%
  dplyr::select(condition, condition.col) %>%
  distinct() -> conddf
cond.col <- conddf$condition.col
names(cond.col) <- conddf$condition
# replace the last col with gray because its hard to see
cond.col[5] <- "#808080" 

# Calculate the percentage of cells that are non-zero by gene, condition, and ITGAX/CD27 group
plotdf.p %>%
  group_by(gene_name, genegrp.condition) %>%
  summarize(n.nonzero = sum(cpm != 0),
            n = length(cell.name),
            perc.nonzero = (n.nonzero / n)*100) %>%
  left_join(level.indx) -> plotdf.perc


GENES <- unique(plotdf.p$gene_name)
sel.gene <- GENES[1]
sel.gene

make_genePlot <- function(plotdf.p, plotdf.perc, sel.gene, save = TRUE, log = FALSE){
  
  # filter to selected gene
  plotdf.p %>%
    filter(gene_name == sel.gene) -> df
  plotdf.perc %>%
    filter(gene_name == sel.gene) -> df2
  
  # plot
  y.pos <- max(df$cpm)*1.1 # position of percentage text
  df %>%
    ggplot(aes(x = condition, y = cpm, color = condition)) +
    geom_point(position = "jitter", pch = 16, size = 1) +
    geom_boxplot(col = "black", alpha = .001) +
    ylab("Counts per million reads mapped (CPM)") + xlab("") +
    ggtitle(sel.gene) +
    theme_classic() +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
    facet_grid(~genegrp) +
    scale_color_manual(values = cond.col) +
    guides(color = "none") +
    geom_text(data = df2, aes(x = condition, y = y.pos, label = round(perc.nonzero, digits = 0))) -> p
outdir <- "Holla2021SciAdv/output/genePlots_cpm"
  
  if(log == TRUE){
    y.pos <- max(log(df$cpm + 1))*1.1 # position of percentage text
    df %>%
      ggplot(aes(x = condition, y = log(cpm + 1), color = condition)) +
      geom_point(position = "jitter", pch = 16, size = 1) +
      geom_boxplot(col = "black", alpha = .001) +
      ylab("Log counts per million reads mapped (CPM)") + xlab("") +
      ggtitle(sel.gene) +
      theme_classic() +
      theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
      facet_grid(~genegrp) +
      scale_color_manual(values = cond.col) +
      guides(color = "none") +
      geom_text(data = df2, aes(x = condition, y = y.pos, label = round(perc.nonzero, digits = 0))) -> p
    
    outdir <- "Holla2021SciAdv/output/genePlots_logcpm"
  }
  
  if(save == TRUE){
    outf <- file.path(outdir, paste0(sel.gene,".png"))
    ggsave(p, filename = outf, dpi = 300, units = "in", 
           width = 6, height = 4)
  }
  return(p)
}
```

Print plots
```{r}
gene.vec <- c("ADA","CCR7","CD72","FCGR2B","FGR",
              "ITGB2","LTB","MPP6","POU2AF1","S1PR1",
              "S1PR2","TNFRSF13B","TRADD")
gene.vec

gene.vec %>%
  map(~make_genePlot(plotdf.p, plotdf.perc, sel.gene = .x))

# log-transformt the y axis
gene.vec %>%
  map(~make_genePlot(plotdf.p, plotdf.perc, sel.gene = .x, log = TRUE))

```

# Make heatmap

## Remove low expression and variability genes

Set up the data matrix and column annotation df. Log-transform CPM first as logcpm = log(cpm + 1)
```{r}
# make gene x cell matrix with cpm values
min.nonzero.cpm <- min(df.list$cpm[df.list$cpm != 0]) #55
constant <- 1
df.list %>%
  mutate(logcpm = log(cpm + constant)) %>%
  dplyr::select(gene_name, cell.name, logcpm) %>%
  pivot_wider(id_cols = gene_name,
              names_from = cell.name,
              values_from = logcpm) -> mat
x.new <- as.matrix(mat[,-c(1)])
rownames(x.new) <- mat$gene_name
dim(x.new) # 127 genes x 4912 cells

# keep track of the cell metadata
df.list %>%
  dplyr::select(cell.name, exp1, itgax.cd27, cohort) %>%
  distinct() %>%
  left_join(level.indx) %>%
  mutate(condition = factor(condition, levels =  c("act_cGVHD","no_cGVHD","Healthy","HIV","Malaria"))) %>%
  mutate(genegrp = factor(genegrp, levels =  c("-ITGAX.+CD27","+ITGAX.-CD27","+ITGAX.+CD27"))) %>%
  mutate(genegrp.condition = factor(genegrp.condition, levels = level.indx$genegrp.condition)) %>%
  remove_rownames() %>%
  column_to_rownames(var = "cell.name") %>%
  arrange(genegrp, condition) -> mat.ann
dim(mat.ann)[1] # 4912 cells

# re-order columns based on trt
x.new %>%
  as_tibble() %>%
  relocate(row.names(mat.ann)) %>%
  as.matrix -> out
row.names(out) <- rownames(x.new)
```

Summarize gene CPM mean and variability across cells
```{r}
out %>%
  data.frame(.) %>%
  rowwise() %>%
  mutate(var = var(c_across(everything())),
         mean = mean(c_across(everything()))) -> out.ov
```

Mean-var plot of the logcpm values
```{r}
min.nonzero.cpm <- min(df.list$cpm[df.list$cpm != 0]) #55

thres = 0.05
data.frame(gene_name = row.names(out),
           var = out.ov$var,
           mean = out.ov$mean) %>%
  ggplot(aes(x = mean, y = var, label = gene_name)) +
  geom_point(size = 1, alpha = .2) +
  geom_text_repel(size = 2) +
  theme_classic() +
  ylab("Variance") + xlab("Mean") +
  geom_vline(xintercept = thres, linetype = 2) +
  geom_hline(yintercept = thres, linetype = 2)
```

Genes with log cpm mean or var less than threshold (0.05)
```{r}
data.frame(gene_name = row.names(out),
           mean = out.ov$mean,
           var = out.ov$var) %>%
  arrange(var, mean) %>%
  mutate(currently.excluded = ifelse(var < thres | mean < thres, "TRUE","FALSE")) -> gene.mv
write.csv(gene.mv, file = "Holla2021SciAdv/output/hmGene-mv_logcpm.csv")

gene.mv %>%
  filter(var < thres | mean < thres) %>%
  pull(gene_name) -> exclude.genes
exclude.genes
```

## Limit the number of cells to display

Randomly select a max of 100 cells from each group. 
```{r}
# how many cells per group?
mat.ann %>%
    group_by(genegrp, condition) %>%
    summarize(ncells = n()) -> ncells.df
sum(ncells.df$ncells) # 4912

# which groups have too many cells?
max.cells <- 100
ncells.df %>%
    filter(ncells > max.cells) -> toomany.df
toomany.df

# for each group with too many cells, randomly select 100
sublist <- list()
for(i in 1:nrow(toomany.df)){
  curr.condition <- as.character(toomany.df[i,"condition"]$condition)
  curr.genegrp <- as.character(toomany.df[i,"genegrp"]$genegrp)
  mat.ann %>%
    rownames_to_column(var = "cell.name") %>%
    filter(condition == curr.condition,
           genegrp == curr.genegrp) %>%
    slice_sample(n = max.cells, replace = F) -> sublist[[i]]
}
sublist %>%
  map_dfr(~data.frame(.x)) -> subsampled.df

# check the number of cells
subsampled.df %>%
  dplyr::select(cell.name, condition, genegrp) %>%
  distinct() %>%
  group_by(condition, genegrp) %>%
  summarize(ncells = length(cell.name))

# update ncells.df with number of subsampled cells
toomany.df %>%
  dplyr::select(-ncells) %>%
  mutate(ncells.sub = 100) -> indx
ncells.df %>%
  left_join(indx) %>%
  mutate(ncells.sub = ifelse(is.na(ncells.sub), ncells, ncells.sub)) -> ncells.df.ann

```

Combine with 700 sub-sampled cells with other cells that did not need to be subsampled
```{r}

# pull out cells that did not need to be subsampled
mat.ann %>%
  rownames_to_column(var = "cell.name") %>%
  left_join(ncells.df.ann) %>%
  filter(ncells.sub != 100) %>%
  dplyr::select(-c(ncells, ncells.sub))-> notsubsampled.df
dim(notsubsampled.df) # 313 cells did not need to be subsampled

# combine
notsubsampled.df %>%
  bind_rows(subsampled.df) -> mat.ann.sub
dim(mat.ann.sub) # 1013 cells to display
length(unique(mat.ann.sub$cell.name)) # 1013

# use mat.ann.sub to subset cells from df.list
df.list %>%
  filter(cell.name %in% mat.ann.sub$cell.name) -> df.list.sub
length(unique(df.list.sub$cell.name)) # 1013 
```

Exclude genes below mean-var threshold. Re-make plotting matrix and column annotation dataframe
```{r}
constant <- 1
df.list.sub %>%
  filter(!gene_name %in% exclude.genes) %>%
  mutate(logcpm = log(cpm + constant)) %>%
  dplyr::select(gene_name, cell.name, logcpm) %>%
  pivot_wider(id_cols = gene_name,
              names_from = cell.name,
              values_from = logcpm) -> mat
x.new <- as.matrix(mat[,-c(1)])
rownames(x.new) <- mat$gene_name
dim(x.new) # 113 genes x 1013 cells

# keep track of the cell metadata
mat.ann.sub %>%
  remove_rownames() %>%
  column_to_rownames(var = "cell.name") %>%
  arrange(genegrp, condition) -> mat.ann
dim(mat.ann)[1] # 1013 cells

# re-order columns based on trt
x.new %>%
  as_tibble() %>%
  relocate(row.names(mat.ann)) %>%
  as.matrix -> out
row.names(out) <- rownames(x.new)
  
# scale by row
out.s <- scale_rows(out)

# save everything needed for plotting
res <- list(mat = out.s, mat.ann = mat.ann)
```

## Order cells by scaled log CPM within condition and genegrp factors

```{r}
# create a long dataframe with the scaled log cpm values and cell metadata
res$mat.ann %>%
  rownames_to_column(var = "cell.name") -> mat.ann
data.frame(res$mat) %>%
  rownames_to_column(var = "gene") %>%
  pivot_longer(cols = -gene,
               names_to = "cell.name",
               values_to = "scaled.logcpm") %>%
  mutate(cell.name = gsub("[.]","-", cell.name)) %>%
  left_join(mat.ann) -> mat.l
length(unique(mat.l$cell.name)) #1013 cells

# loop through each genegrp and condition to sort cells
GRP <- unique(mat.ann$genegrp.condition)
GENE <- unique(mat.l$gene)
length(GRP) * length(GENE) # 15 grps x 113 genes = 1695
grid <- expand_grid(GENE,GRP)
dim(grid) #1695
grid$gridrow <- as.character(seq.int(from = 1, to = nrow(grid)))
sorted.df.list <- list()
for(i in 1:nrow(grid)){
  grid %>%
    filter(gridrow == i) %>%
    pull(GRP) %>% as.character() -> curr.grp
  grid %>%
    filter(gridrow == i) %>%
    pull(GENE) %>% as.character() -> curr.gene
  mat.l %>%
    filter(genegrp.condition == curr.grp,
           gene == curr.gene) %>%
    arrange(scaled.logcpm) %>%
    dplyr::select(genegrp.condition, gene, cell.name, scaled.logcpm) -> sorted.df
  sorted.df$cell.order <- seq.int(from = 1, to = nrow(sorted.df))
  sorted.df.list[[i]] <- sorted.df
}
names(sorted.df.list) <- paste0("gridrow",grid$gridrow)
# sorted.df.list %>%
#   map(~dim(.x))

# reshape sorted.df.list into matrix with rows = genes, cols = cells (ordered by genegrp.condition), values = scaled log cpm (ordered by rank within gene and genegrp.condition)
# fill the matrix by rows
curr.row <- list()
curr.names <-list()
for(i in 1:length(GENE)){
  
  # pull out elements of sorted.df.list that correspond to the selected gene
  sel <- which(grid$GENE == GENE[i])
  
  # grab the scaled.logcpm values, sorted within genegrp.condition
  sorted.df.list[sel] %>%
    map_dfr(~data.frame(.x)) %>%
    pull(scaled.logcpm) -> curr.row[[i]]
  
  # keep track of the cell metadata at the genegrp.condition level
  sorted.df.list[sel] %>%
    map_dfr(~data.frame(.x)) %>%
    pull(genegrp.condition) -> curr.names[[i]]
}
names(curr.row) <- GENE
names(curr.names) <- GENE

mat.sorted <- matrix(data = unlist(curr.row), 
                     nrow = length(GENE), 
                     ncol = length(curr.names[[1]]), byrow = T)
row.names(mat.sorted) <- GENE
#sum(curr.names[[1]] != curr.names[[2]]) # these should match
colnames(mat.sorted) <- paste0("col", 1:length(curr.names[[1]]), "__", curr.names[[1]])
dim(mat.sorted) # 113 rows (genes) x 1013 columns (cells)

# identify the columns of mat.sorted
data.frame(col.name = colnames(mat.sorted)) %>%
  separate(col.name, into = c("col.num","genegrp.condition"), sep ="__", remove = F) %>%
  mutate(genegrp.condition = factor(genegrp.condition, 
                                    levels = unique(mat.ann$genegrp.condition))) -> mat.ann.sorted

# colors for tiles
quants <- quantile(mat.sorted, c(.05, .90))
col_fun = colorRamp2(c(quants[1], 0, quants[2]), c("blue", "white", "red"))

# top annotation
ca <- columnAnnotation(
  group = anno_block(
    graphics = function(index, levels) {
      grid.rect(gp = gpar(fill = genegrp.col[levels], col = genegrp.col[levels]))
      grid.text(label = paste(genegrp.txt[levels], collapse = ","), 
                x = 0.5, y = 0.5, rot = 0, 
                gp = gpar(col = 1, fontsize = 6))
    }
  ),
  condition = anno_block(
    graphics = function(index, levels) {
      grid.rect(gp = gpar(fill = condition.col[levels], col = condition.col[levels]))
      grid.text(label = paste(condition.txt[levels], collapse = ","), 
                x = 0.5, y = 0.5, rot = 0, 
                gp = gpar(col = 1, fontsize = 6))
    }
  )
)

p <- Heatmap(mat.sorted, 
             col = col_fun, name = "scaled logCPM", column_title = " ",
             cluster_rows = FALSE, cluster_columns = FALSE, cluster_column_slices = FALSE,
             show_column_names = FALSE,
             row_names_side = "left",
             row_names_gp = gpar(fontsize = 6),
        column_split = mat.ann.sorted$genegrp.condition,
        top_annotation = ca)
p

png(file = "Holla2021SciAdv/output/cpm_max100cells_removeLowMV_sorted.png",
    width = 15, height = 10, units = "in", res = 600)
draw(p)
dev.off()
```
