---
title: "test-integration"
output: html_document
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE, warning = FALSE, cache = T)
```

# Prerequisites

Load packages

```{r pkg}
library(tidyverse)
library(readxl)
library(kableExtra)
library(Seurat)
library(yaml)
```

Load paths

```{r filedirs}
wd <- "/mnt/data1/workspace/Sarantopoulos/cGVHD/Code/Holla2021SciAdv"

myconf <- yaml::read_yaml(file.path(wd, 'config.yml'))
procdir <- myconf$procdir
codedir <- myconf$codedir
holladir <-  myconf$holladir
poedir <- myconf$poedir
# gtfdir <- myconf$gtfdir
# gfile <- file.path(
#   gtfdir,'gencode.v24.chr_patch_hapl_scaff.basic.annotation.gtf'
# )
# tools::md5sum(gfile)
```

Import Utility functions
```{r utilfun}
utilfname <- file.path(codedir, "util.R") 
tools::md5sum(utilfname)
source(utilfname)
```

Import gtf file subsetted to featureids in the datasets. See 2_make-cellassign-signature.Rmd for how this intermediate was created.
```{r gtfimport}
gtf <- readRDSobj(procdir, "gtf-featureids.RDS")
```

Identify study gene list
```{r}
xlfile <- file.path(codedir,"data","Signature gene list for new figure_v3-18-2022_revised.xlsx")
```


# Load seurat data

The data intermediate `bcellobj-poe-holla.RDS` was created in `5_integrate-cohorts.Rmd`
```{r}
#bcellobj <- readRDSobj(procdir, "bcellobj-poe-holla.RDS")
```


# Import and check the heatmap gene list

Import heatmap genes
```{r importHeatGenes}
readxl::read_xlsx(
  xlfile,
  skip = 1,
  col_names = "gene_name"
) %>%
  pull(gene_name) %>% unique() -> glist
length(glist) # 126 unique symbols
```

Make sure that PAX5, ITGAX, and CD27 are present in the heatmap gene list. Need to add PAX5
```{r}
c("PAX5","ITGAX","CD27") %in% glist

#' Add PAX5
glist.ann <- union(glist, 'PAX5')
```

Use the GTF to annotate the gene symbols with Ensembl IDs
```{r}
# Check that all glist symbols are in the gtf
glist.ann[!glist.ann %in% gtf$gene_name]

# Add ensembl ids to glist
data.frame(gene_name = glist.ann) %>%
  dplyr::inner_join(gtf, by = "gene_name") -> genepanel
dim(genepanel) #148
```

Identify cases where there are multiple ensembl ids for 1 gene symbol. 12 gene symbols.
```{r}
genepanel %>%
  group_by(gene_name) %>%
  summarize(n = length(unique(gene_id)),
            cat.geneid = paste0(gene_id, collapse = "__")) %>%
  dplyr::filter(n != 1) %>%
  dplyr::arrange(n) -> redund.symb # 12 gene_names
redund.symb
```

Identify cases where there are multiple gene symbols for 1 ensembl id. None.
```{r}
genepanel %>%
  group_by(gene_id) %>%
  summarize(n = length(unique(gene_name)),
            cat.gene_name = paste0(gene_name, collapse = "__")) %>%
  dplyr::filter(n != 1) %>%
  dplyr::arrange(n) # 0 gene_ids
```



# Integrate datasets

## Normalize and identify variable features for each dataset independently
```{r}
bcellobj.norm <- lapply(X = bcellobj, FUN = function(x) {
    x <- Seurat::NormalizeData(x)
    x <- Seurat::FindVariableFeatures(x, selection.method = "vst", nfeatures = 2000)
})
```



## 1. Fast integration, all cohorts

### Select integration features
```{r}
# select features that are repeatedly variable across datasets for integration
varfeatures <- Seurat::SelectIntegrationFeatures(object.list = bcellobj.norm)

#' Check for intersection with the variable features
#' within each cohort
lapply(
  bcellobj.norm,
  function(x) {
    length(intersect(Seurat::VariableFeatures(x), varfeatures))
  }
)
```

### Add heatmap genes to feature set
```{r}
length(varfeatures)
myfeatures <- union(varfeatures, genepanel$geneid)
length(myfeatures)
```

### Identify anchor features 
```{r}
# this is needed to run "rpca", otherwise you get...
# Error: Cannot find 'pca' in this Seurat object
#' Scale and run PCA on each cohort limited to these features
lapply(
  X = bcellobj.norm,
  FUN = function(x) {
    x <- Seurat::ScaleData(x, features = myfeatures, verbose = FALSE)
    x <- Seurat::RunPCA(x, features = myfeatures, verbose = FALSE)
  }
) -> integratedcohort

#' Find integration anchors
Seurat::FindIntegrationAnchors(
  integratedcohort,
  anchor.features =  myfeatures,
  reduction = "rpca"
) -> anchorfeatures3
```

### Integrate
```{r}
Seurat::IntegrateData(
  anchorset = anchorfeatures3,
  features.to.integrate = myfeatures,
  new.assay.name = "integrated",
  normalization.method = "LogNormalize",
  dims = 1:30
) -> intFast

# if you omit the line "features.to.integrate = myfeatures", you get the error:
# Error in h(simpleError(msg, call)) : error in evaluating the argument 'x' in selecting a method for function 't': invalid character indexing

# if you include the line, you get this warning:
# Warning: Not all features provided are in this Assay object, removing the following feature(s): ENSG00000282633, ENSG00000276173, ENSG00000278801, ENSG00000277633, ENSG00000274497, ENSG00000282184, ENSG00000277016, ENSG00000282657, ENSG00000274669, ENSG00000277807, ENSG00000276452, ENSG00000277134, ENSG00000231314, ENSG00000204487, ENSG00000236925, ENSG00000223448, ENSG00000236237, ENSG00000206437, ENSG00000238114, ENSGR0000182162, ENSG00000276977

Seurat::DefaultAssay(
  intFast
) <- "integrated"

saveRDSobj(intFast, procdir, "poe-holla")
#intFast <- readRDSobj(procdir, "intFast-poe-holla.RDS")
```

## 2. Fast integration, remove Poe.1 cohort

### Remove Poe1.cohort
```{r}
names(bcellobj.norm)
bcellobj.no1 <- bcellobj.norm[-1]
names(bcellobj.no1)
```

### Select integration features and add heatmap genes to feature set
```{r}
# select features that are repeatedly variable across datasets for integration
varfeatures <- Seurat::SelectIntegrationFeatures(object.list = bcellobj.no1)

#' Check for intersection with the variable features
#' within each cohort
lapply(
  bcellobj.no1,
  function(x) {
    length(intersect(Seurat::VariableFeatures(x), varfeatures))
  }
)

length(varfeatures)
myfeatures <- union(varfeatures, genepanel$geneid)
length(myfeatures)
```

### Identify anchor features and integrate
```{r}
# this is needed to run "rpca", otherwise you get...
# Error: Cannot find 'pca' in this Seurat object
#' Scale and run PCA on each cohort limited to these features
lapply(
  X = bcellobj.no1,
  FUN = function(x) {
    x <- Seurat::ScaleData(x, features = myfeatures, verbose = FALSE)
    x <- Seurat::RunPCA(x, features = myfeatures, verbose = FALSE)
  }
) -> integratedcohort

#' Find integration anchors
Seurat::FindIntegrationAnchors(
  integratedcohort,
  anchor.features =  myfeatures,
  reduction = "rpca"
) -> anchorfeatures

Seurat::IntegrateData(
  anchorset = anchorfeatures,
  features.to.integrate = myfeatures,
  new.assay.name = "integrated",
  normalization.method = "LogNormalize",
  dims = 1:30
) -> intNoPoe1


Seurat::DefaultAssay(
  intNoPoe1
) <- "integrated"

saveRDSobj(intNoPoe1, procdir, "poe-holla")
#intNoPoe1 <- readRDSobj(procdir, "intNoPoe1-poe-holla.RDS")
```


## 3. Fast integration, remove Poe.2 cohort

### Remove Poe2.cohort
```{r}
names(bcellobj.norm)
bcellobj.no2 <- bcellobj.norm[-2]
names(bcellobj.no2)
```

### Select integration features and add heatmap genes to feature set
```{r}
# select features that are repeatedly variable across datasets for integration
varfeatures <- Seurat::SelectIntegrationFeatures(object.list = bcellobj.no2)

#' Check for intersection with the variable features
#' within each cohort
lapply(
  bcellobj.no2,
  function(x) {
    length(intersect(Seurat::VariableFeatures(x), varfeatures))
  }
)

length(varfeatures)
myfeatures <- union(varfeatures, genepanel$geneid)
length(myfeatures)
```

### Identify anchor features and integrate
```{r}
# this is needed to run "rpca", otherwise you get...
# Error: Cannot find 'pca' in this Seurat object
#' Scale and run PCA on each cohort limited to these features
lapply(
  X = bcellobj.no2,
  FUN = function(x) {
    x <- Seurat::ScaleData(x, features = myfeatures, verbose = FALSE)
    x <- Seurat::RunPCA(x, features = myfeatures, verbose = FALSE)
  }
) -> integratedcohort

#' Find integration anchors
Seurat::FindIntegrationAnchors(
  integratedcohort,
  anchor.features =  myfeatures,
  reduction = "rpca"
) -> anchorfeatures

Seurat::IntegrateData(
  anchorset = anchorfeatures,
  features.to.integrate = myfeatures,
  new.assay.name = "integrated",
  normalization.method = "LogNormalize",
  dims = 1:30
) -> intNoPoe2

Seurat::DefaultAssay(
  intNoPoe2
) <- "integrated"

saveRDSobj(intNoPoe2, procdir, "poe-holla")
#intNoPoe2 <- readRDSobj(procdir, "intNoPoe2-poe-holla.RDS")
```

# Analyze integrated data

Scale, RunPCA, RunUMAP, FindNeighbors, and FindClusters
```{r analyze}
# custom function
process_integratedObj <- function(integratedcohort){
  
  Seurat::ScaleData(
    integratedcohort,
    verbose = FALSE
  ) -> integratedcohort
  #'
  Seurat::RunPCA(
    integratedcohort,
    npcs = 30,
    verbose = FALSE
  ) -> integratedcohort
  #'
  Seurat::RunUMAP(
    integratedcohort,
    reduction = "pca",
    dims = 1:30
  ) -> integratedcohort
  #'
  Seurat::FindNeighbors(
    integratedcohort,
    reduction = "pca",
    dims = 1:30
  ) -> integratedcohort
  #'
  Seurat::FindClusters(
    integratedcohort,
    resolution = 0.5
  ) -> integratedcohort
  
  return(integratedcohort)
  
}

# run for each integrated object

# 1. Fast integration, all cohorts
intFast <- process_integratedObj(intFast)
intFast
saveRDSobj(intFast, procdir, "poe-holla")
# fastAll: LEFT OFF HERE!

# 2. Fast integration, remove Poe1 cohort
intNoPoe1 <- process_integratedObj(intNoPoe1)
intNoPoe1
saveRDSobj(intNoPoe1, procdir, "poe-holla")
# fastNoPoe1: LEFT OFF HERE

# 3. Fast integration, remove Poe2 cohort
intNoPoe2 <- process_integratedObj(intNoPoe2)
intNoPoe2
saveRDSobj(intNoPoe2, procdir, "poe-holla")
# fastNoPoe2: LEFT OFF HERE


```



# Session Information
```{r sessinfo}
sessionInfo()
```


