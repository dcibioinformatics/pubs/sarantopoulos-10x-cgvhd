---
title: "Poe et al and Holla et al: Cell type annotation "
author: "DCI Bioinformatics"
date: '`r format(Sys.Date(), "%B %d, %Y")`'
output:
  html_document:
    toc: true
    toc_depth: 3
    toc_float: true
    code_folding: show
---

# Packages
```{r pkg}
library(tidyverse)
library(kableExtra)
```

```{r filedirs}
wd <- "/mnt/data1/workspace/Sarantopoulos/cGVHD/Code/Holla2021SciAdv"

myconf <- yaml::read_yaml(file.path(wd, 'config.yml'))
procdir <- myconf$procdir
codedir <- myconf$codedir
holladir <-  myconf$holladir
poedir <- myconf$poedir
gtfdir <- myconf$gtfdir
gfile <- file.path(
  gtfdir,'gencode.v24.chr_patch_hapl_scaff.basic.annotation.gtf'
)
tools::md5sum(gfile)
```

# Import Utility functions
```{r utilfun}
utilfname <- file.path(codedir, "util.R") 
tools::md5sum(utilfname)
source(utilfname)
```

# Celltype signature from cellassign pkg (Zhang et al, Nature Methods 2019)
```{r cellassigndata, echo = FALSE}
cassignsig <- structure(list(Group = c(
  "B cells", "B cells", "B cells", "B cells",
  "B cells", "B cells", "T cells", "T cells", "T cells", "T cells",
  "T cells", "T cells", "T cells", "T cells", "Cytotoxic T cells",
  "Cytotoxic T cells", "Cytotoxic T cells", "Cytotoxic T cells",
  "Cytotoxic T cells", "Cytotoxic T cells", "Cytotoxic T cells",
  "Cytotoxic T cells", "Cytotoxic T cells", "Cytotoxic T cells",
  "Cytotoxic T cells", "Monocyte/Macrophage", "Monocyte/Macrophage",
  "Monocyte/Macrophage", "Monocyte/Macrophage", "Monocyte/Macrophage",
  "Monocyte/Macrophage", "Monocyte/Macrophage", "Monocyte/Macrophage",
  "Monocyte/Macrophage", "Epithelial cells", "Epithelial cells",
  "Epithelial cells", "Epithelial cells", "Myofibroblast", "Myofibroblast",
  "Myofibroblast", "Myofibroblast", "Myofibroblast", "Vascular smooth muscle cells",
  "Vascular smooth muscle cells", "Vascular smooth muscle cells",
  "Vascular smooth muscle cells", "Vascular smooth muscle cells",
  "Vascular smooth muscle cells", "Vascular smooth muscle cells",
  "Vascular smooth muscle cells", "Vascular smooth muscle cells",
  "Endothelial cells", "Endothelial cells", "Endothelial cells",
  "Endothelial cells", "Endothelial cells", "Endothelial cells",
  "Endothelial cells", "Endothelial cells"
), Marker = c(
  "VIM",
  "MS4A1", "CD79A", "PTPRC", "CD19", "CD24", "VIM", "CD2", "CD3D",
  "CD3E", "CD3G", "CD28", "CD4", "PTPRC", "VIM", "CD2", "CD3D",
  "CD3E", "CD3G", "CD28", "CD8A", "GZMA", "PRF1", "NKG7", "PTPRC",
  "VIM", "CD14", "FCGR3A", "CD33", "ITGAX", "ITGAM", "CD4", "PTPRC",
  "LYZ", "EPCAM", "CDH1", "CLDN3", "CLDN4", "VIM", "ACTA2", "COL1A1",
  "COL3A1", "SERPINH1", "VIM", "ACTA2", "MYH11", "PLN", "MYLK",
  "MCAM", "COL1A1", "COL3A1", "SERPINH1", "VIM", "EMCN", "CLEC14A",
  "CDH5", "PECAM1", "VWF", "MCAM", "SERPINH1"
), Weight = c(
  2, 2,
  2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
  2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
  2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
)), row.names = c(
  NA,
  -60L
), class = "data.frame")

head(cassignsig)
```

# Limit gtf gene info to ids present in study data
```{r gtfids}
hollacnt <- readRDSobj(procdir, "hollacnt-rawcntobj.RDS")
#' 
poecnt <- readRDSobj(procdir, "poecnt-rawcntobj.RDS")
#'
featureids <- union(hollacnt[["featureids"]], poecnt[["featureids"]])
```

# Import gtf file
```{r gtfimport}
rtracklayer::import(gfile) %>%
  plyranges::filter(type == "gene") %>%
  tibble::as_tibble() %>%
  plyranges::select(gene_id, gene_name) %>%
  dplyr::mutate(geneid = gsub(pattern = "\\.\\d+$", "", gene_id)) -> gtf

gtf %>%
  dplyr::filter(geneid %in% featureids) ->
  gtf

length(featureids) == nrow(gtf)

head(gtf)
```

```{r celltype}
#' Identify any symbols from ctype not in gtf
setdiff(cassignsig$Marker, gtf$gene_name)
#' Get ensembl ids for genes in cell type signature
gtf %>%
  dplyr::filter(gene_name %in% cassignsig$Marker) %>%
  dplyr::full_join(cassignsig, by = c("gene_name" = "Marker")) ->
  geneanno

geneanno %>%
  kableExtra::kbl(caption = "Cell type Annotation") %>%
  kableExtra::kable_classic()

geneanno %>%
  dplyr::transmute(Group, Marker = geneid, Weight) %>%
  dplyr::arrange(Group) %>%
  as.data.frame() ->
  ctypesig

ctypesig %>%
  kableExtra::kbl(caption = "Cell type signature object") %>%
  kableExtra::kable_classic()
```

# Save gtf that has been subsetted to ids present in study data
```{r}
saveRDSobj(gtf, procdir, "featureids")
```

# Save Data object
```{r saveobj}
saveRDSobj(ctypesig, procdir, "cellassign-signature")
```

# Session Information
```{r sessinfo}
sessionInfo()
```
