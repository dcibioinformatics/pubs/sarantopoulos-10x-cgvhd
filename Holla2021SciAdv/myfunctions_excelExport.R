# Custom functions to export tables to xlsx and add metadata sheets

##### Functions #####
add_labelleft <- function(mat, label){
  mat.nrows <- dim(mat)[1]
  mat.tmp <- cbind(matrix(NA, nrow = mat.nrows), mat)
  mat.tmp[1,1] <- label
  return(mat.tmp)
}

widen_matrix <- function(mat, colsNeeded){
  curr.rows <- dim(mat)[1]
  mat.w <- cbind(mat,
                 matrix(rep(NA, curr.rows * colsNeeded), 
                        nrow = curr.rows, byrow = T))
  return(mat.w)
}

combine_metadataPage <- function(preamble, notes, sheets, cols){
  
  # add labels
  notes.lab <- add_labelleft(notes, "Notes")
  sheets.lab <- add_labelleft(sheets, "Sheets")
  cols.lab <- add_labelleft(cols, "Columns")
  
  # find the max num columns needed
  max.cols <- max(dim(preamble)[2], dim(notes.lab)[2], dim(sheets.lab)[2], dim(cols.lab)[2])
  
  # widen so all elements are the same width
  preamble.colsNeeded <- max.cols - dim(preamble)[2]
  if(preamble.colsNeeded > 0){
    preamble <- widen_matrix(mat = preamble, preamble.colsNeeded)
  }
  notes.colsNeeded <- max.cols - dim(notes.lab)[2]
  if(notes.colsNeeded > 0){
    notes.lab <- widen_matrix(mat = notes.lab, notes.colsNeeded)
  }
  sheets.colsNeeded <- max.cols - dim(sheets.lab)[2]
  if(sheets.colsNeeded > 0){
    sheets.lab <- widen_matrix(mat = sheets.lab, sheets.colsNeeded)
  }
  cols.colsNeeded <- max.cols - dim(cols.lab)[2]
  if(cols.colsNeeded > 0){
    cols.lab <- widen_matrix(mat = cols.lab, cols.colsNeeded)
  }
  
  # rbind with empty lines in between each element
  full.mat <- rbind(preamble, NA,
                    notes.lab, NA,
                    sheets.lab, NA,
                    cols.lab, NA)
  
  return(full.mat)
}


##### Example, see 3_RNASeqPaths_Force-ERCC.Rmd #####

#library(openxlsx)

# # 1. add raw data
# essetdf.list <- list(star = data.frame("testing1"), salmon = data.frame("testing2"))
# wb <- openxlsx::buildWorkbook(essetdf.list, asTable = F)
# wb %>%
#   openxlsx::addWorksheet(sheetName = "metadata")
# openxlsx::worksheetOrder(wb) <- c(3, 1, 2)
# 
# # 2. build the elements of the metadata sheet
# 
# preamble <- matrix(c("Significant pathway sets based on gage analysis of treatment within each cell line", 
#                      "Project: Force-ERCC-Cellline-RNAseq", 
#                      "Author: Marissa Lee", 
#                      date()),
#                    nrow = 4, byrow = T)
# 
# notes.meta <- matrix(c("Significant pathways are grouped together into pathway sets based on the similarity of their core genes using tools the GAGE R package. Core genes are defined as having a mean expression greater than 1 standard deviation of the mean value of all genes in the direction of interest. For each pathway set, the pathway with the largest effect size is considered the `representative pathway`",
#                        "See the report `3_RNASeqPaths_Force-ERCC.html` section `Pathway sets` for the corresponding tile plots"),
#                      nrow = 2, byrow = T)
# 
# sheet.meta <- matrix(c("name","description",
#                        "star","Pathway sets based on STAR read mapping and quantification",
#                        "salmon", "Pathway sets based on Salmon read mapping and quantification"), 
#                      nrow = 3, byrow = T)
# 
# #colnames(essetdf.star.out)
# col.meta <- matrix(c("cline","Cell line",
#                      "set.name","Pathway set name",
#                      "pathway","Pathway name", 
#                      "stat.mean","From gage, mean of the test statistics associated with each gene in the pathway. Test statistics are equal to log2FoldChange/lfcSE",
#                      "representative.pathway","Representative pathway in the set; has the largest effect size"),
#                    nrow = 5, byrow = T)
# 
# # 3. combine the elements into 1 matrix that can be printed out to an excel sheet
# full.mat <- combine_metadataPage(preamble = preamble, notes = notes.meta, sheets = sheet.meta, cols = col.meta)
# openxlsx::writeData(wb, sheet = "metadata", x = full.mat,
#                     startCol = 1,
#                     startRow = 1,
#                     rowNames = F, colNames = F)
# 
# # 4. export the workbook as an excel file
# openxlsx::saveWorkbook(wb, "output/pathwaySets.xlsx", overwrite = TRUE)
