ranger=/mnt/data1/workspace/Sarantopoulos/cGVHD/Programs/cellranger-2.1.1/cellranger
datdir=/mnt/data1/staging/Sarantopoulos/cGVHD/RawData
fastqdir=/mnt/data1/workspace/Sarantopoulos/cGVHD/Proc/bcl2fastq
$ranger mkfastq --id=bcl2fastq \
    --run=${datdir}/raw_bcl  \
    --csv=${datdir}/Sarantoupolous-Poe_scRNA_samplesheet.csv \
    --delete-undetermined \
    --output_dir=${fastqdir}/ \
    --localcores 6 

